# wiremock-esb

Start mock with docker
 > "docker-compose up -d --force-recreate --build"

Start mock with standalone
 > - Descargar wiremock-standalone ( http://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-standalone/2.17.0/wiremock-standalone-2.17.0.jar )
 > - Mover standalone a wiremock/
 > - Desde consola de comando parado en la carpeta wiremock-esb/ correr "java -jar wiremock-standalone-2.17.0.jar"